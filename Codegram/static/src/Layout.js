import React from 'react'
import {Route, Switch} from "react-router-dom";
import {Footer, Header, Home, Login, Messages, NotFound, Projects} from 'src/components'
// import {urls} from "src/configs";
import 'src/styles/body.less'

const routes = {
	urls: {
		home: {
			exact: true,
			path: "/",
			name: "Home",
			component: Home
		},
		projects: {
			path: "/projects",
			name: "Projects",
			component: Projects,
		},
		messages: {
			path: "/messages",
			name: "Messages",
			component: Messages
		},
	},
	Header: {
		component: Header
	},
	Footer: {
		component: Footer
	},
	NotFound: {
		component: NotFound
	}
};

class Layout extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			routes: routes
		}
	}

	render() {
		const {urls, NotFound} = this.state.routes;
		const Header = this.state.routes.Header.component;
		const Footer = this.state.routes.Footer.component;

		return (
			<Switch>
				<Route exact path={"/login"} component={Login}/>
				<Route render={() => (
					<div>
						<Header urls={urls}/>
						<Switch>
							{Object.entries(urls).map((url, key) => (
								<Route key={`route-${key}`}
									   exact={url[1].exact}
									   path={url[1].path}
									   component={url[1].component}/>
							))}
							<Route component={NotFound.component}/>
						</Switch>
						<Footer/>
					</div>
				)}/>
			</Switch>
		)
	}
}

export default Layout;