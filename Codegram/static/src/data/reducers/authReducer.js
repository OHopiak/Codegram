import {TOKEN_CLEAR, TOKEN_GET} from "../actions/types";

const initState = {
	loggedIn: false
};

const authReducer = (state = initState, action) => {
	switch (action.type) {
		case TOKEN_GET:
			return {...state, token: action.payload.token, loggedIn: true};
		case TOKEN_CLEAR:
			return {...state, loggedIn: false};
		default:
			return state
	}
};

export default authReducer;