import {TOKEN_CLEAR, TOKEN_GET} from "./types";

const getToken = ({username, password}) => ({
	type: TOKEN_GET,
	payload: {
		// url: "/api/auth/token/",
		ref: "token",
		method: "POST",
		body: {username, password}
	}
});

const clearToken = () => ({
	type: TOKEN_CLEAR
});

export {
	getToken,
	clearToken,
}