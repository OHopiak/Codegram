import {API_PREFIX} from "src/data/actions/types";
import axios from "axios";

const actionIsForApi = action => (
	action.type.startsWith(API_PREFIX)
	&& action.payload
	&& (action.payload.url || action.payload.ref)
);

const NOP_ACTION = {type: "NOP"};

const apiMiddleware = config => store => next => action => {
	if (!actionIsForApi(action)) {
		next(action);
		return
	}

	let client = axios;

	if (store.auth && store.auth.token) {
		client = axios.create({
			headers: {
				common: {
					Authentication: `JWT ${store.auth.token}`
				}
			}
		})
	}

	let request = null;
	let url = null;

	if (action.payload.url)
		url = action.payload.url;
	else if (action.payload.ref)
		url = config.urls[action.payload.ref];
	else {
		next(NOP_ACTION);
		return
	}

	const requestData = action.payload.body;
	const method = action.payload.method ? action.payload.method : "GET";

	switch (method) {
		case "GET":
			request = client.get(url);
			break;
		case "POST":
			request = client.post(url, requestData);
			break;
		case "PUT":
			request = client.put(url, requestData);
			break;
		case "DELETE":
			request = client.delete(url, requestData);
			break;
		default:
			next(NOP_ACTION);
			return
	}

	request
		.then(response => {
			next({type: action.type, payload: response.data})
		})
		.catch(error => {
			console.error(error);
			next(NOP_ACTION)
		})
};

export default apiMiddleware