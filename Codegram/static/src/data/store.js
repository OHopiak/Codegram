import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
// import {persistStore} from 'redux-persist';
// import promise from 'redux-promise-middleware'
// import thunk from 'redux-thunk'
import reducer from './reducers/index'
import {apiMiddleware} from "./middleware";
import {apiUrls} from "src/configs/urls";


const logger = createLogger({
	collapsed: true,
});
const api = apiMiddleware({
	urls: apiUrls
});

const middleware = applyMiddleware(
	logger,
	api,
	// promise(),
	// thunk,
);

const store = createStore(reducer, middleware);

// persistStore(store);

export default store;