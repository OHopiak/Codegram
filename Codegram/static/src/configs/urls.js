const urls = {
	home: {
		exact: true,
		path: "/",
		name: "Home",
	},
	friends: {
		path: "/friends",
		name: "Projects",
	},
	messages: {
		path: "/messages",
		name: "Messages",
	}
};

const apiUrls = {
	token: "/api/auth/token/"
};

export default urls
export {
	apiUrls
}