import React from 'react'
import {BrowserRouter as Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import Layout from "src/Layout";
import store from "src/data/store";

const App = () => (
	<Provider store={store}>
		<Router>
			<Layout/>
		</Router>
	</Provider>
);

export default App;