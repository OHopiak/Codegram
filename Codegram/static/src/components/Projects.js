import React from 'react'
import contentBase from './decorators/contentBase';
import 'src/styles/projects.less'
import {Link} from "react-router-dom";

const projectsMock = [
	{
		name: "FastChat",
		language: "java",
		framework: "JavaSE",
		type: "jar",
		sites: [
			{
				name: "docs",
				path: "docs/index.html"
			},
			{
				name: "tests",
				path: "tests/index.html"
			},
		],
		versions: [
			"0.0.3",
			"0.0.2",
			"0.0.1",
		],
	}, {
		name: "CodeGen",
		language: "go",
		type: "exe",
		sites: [
			{
				name: "docs",
				path: "docs/index.html"
			},
			{
				name: "tests",
				path: "tests/index.html"
			},
		],
		versions: [
			"0.0.1",
		],
	},
];

const ProjectImage = ({name, thumbnail}) => {
	if (thumbnail) return (
		<div className="friend-picture">
			<img src={thumbnail}/>
		</div>
	);
	else return (
		<div className="friend-letter">
			<span>{(name[0] + name[1]).toUpperCase()}</span>
		</div>
	)
};

const ProjectRow = ({name, thumbnail, versions, sites}) => (
	<div className="project-row">
		<div>
			<ProjectImage name={name} thumbnail={thumbnail}/>
			<p>{name} <Link to={`/data/${name}/${versions[0]}/${sites[0].path}`} target="_blank">Docs</Link></p>
		</div>
	</div>
);

const ProjectTile = ({name, thumbnail, versions, sites}) => (
	<div className="project-tile">
		<div>
			<p>{name} <Link to={`/data/${name}/${versions[0]}/${sites[0].path}`} target="_blank">Docs</Link></p>
		</div>
	</div>
);

const ProjectItem = ({itemType, ...props}) => {
	if (itemType === "row") return <ProjectRow {...props}/>;
	else if (itemType === "tile") return <ProjectTile {...props}/>;
	else return <div/>
};

@contentBase("Projects")
class Projects extends React.Component {
	render() {
		return (
			<div className="project-list">
				{projectsMock.map((friend, key) => (
					<ProjectItem key={`friend-${key}`} itemType={"row"} {...friend}/>
				))}
			</div>
		)
	}
}

export default Projects