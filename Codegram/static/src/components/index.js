import Home from './Home';
import Header from './Header'
import Footer from './Footer'
import Projects from './Projects'
import Messages from './Messages';
import NotFound from './NotFound';
import Login from './Login';

export {
	Home,
	Header,
	Footer,
	Projects,
	Messages,
	NotFound,
	Login,
}