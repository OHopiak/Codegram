import React from 'react'
import contentBase from "src/components/decorators/contentBase";

const NotFound = contentBase("404")(({match}) => (
	<h2>Page <code>{match.path}</code> is not found</h2>
));

export default NotFound
