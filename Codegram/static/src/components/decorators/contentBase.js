import React from 'react'

const contentBase = title => Child => props => (
	<div className="page">
		<div className="heading">
			<div role="main">
				<h1>{title}</h1>
			</div>
		</div>
		<div className="content">
			<Child {...props}/>
		</div>
	</div>
);

export default contentBase