import React from 'react'
import 'src/styles/footer.less'

const Footer = () => (
	<div className="footer">
		<p className="site-credits">© Orest Hopiak</p>
	</div>
);

export default Footer;