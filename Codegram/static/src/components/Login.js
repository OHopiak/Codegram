import React from 'react'
import {connect} from "react-redux";
import 'src/styles/login.less'
import {getToken} from "src/data/actions";
import {Redirect} from "react-router-dom";

const setupStore = (store) => ({
	auth: store.auth
});

const setupDispatcher = (dispatch) => ({
	getToken: (body) => dispatch(getToken(body))
});

@connect(setupStore, setupDispatcher)
class Login extends React.Component {

	/**
	 * Gets username and password from the form for which this event is triggered
	 * @param event form submitted
	 */
	onSubmit = (event) => {
		event.preventDefault();
		let data = {};
		Array.from(event.target.children).forEach(item => {
			if (item.name) data[item.name] = item.value
		});
		this.props.getToken(data);
	};

	/*
	onInputChange = (event) => {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		this.setState({
			[name]: value
		});
	};
	*/

	render() {
		if (this.props.auth.loggedIn) {
			const params = new URLSearchParams(this.props.location.search);
			let next = params.get('next');
			if (!next) next = "/";
			return <Redirect to={next}/>;
		}
		else
			return (
				<div>
					<div className="login-header">
						<h1>Codegram</h1>
					</div>
					<div className="login-form-wrapper">
						<form className="login-form" onSubmit={this.onSubmit}>
							<h2>Login Form</h2>
							<input type="text" name="username" placeholder="username" className="field"/>
							<input type="password" name="password" placeholder="password" className="field"/>

							<label>Remember me</label>
							<input type="checkbox" name="remember"/>

							<div><input type="submit" value="Log In"/></div>
						</form>
					</div>
				</div>
			)
	}
}

export default Login