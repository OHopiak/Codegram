import React from 'react'
import {Link} from "react-router-dom";
import 'src/styles/header.less'

class Header extends React.Component {
	toggleCollapse = () => {
		this.setState({
			collapsed: !this.state.collapsed
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			collapsed: false
		}
	}

	render() {
		return (
			<div className={`header${this.state.collapsed ? " collapsed" : ""}`}>
				<p className="logo"><Link to="/">Codegram</Link></p>
				<div>
					<div className="menu-items">
						{Object.entries(this.props.urls).map((url, key) => (
							<Link to={url[1].path} key={`link-${key}`}>
								<div className="menu-entry">
									{url[1].name}
								</div>
							</Link>
						))}
					</div>
				</div>
				{/*<span onClick={this.toggleCollapse}>C</span>*/}
			</div>
		)
	}
}

export default Header;