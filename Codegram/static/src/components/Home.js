import React from 'react'
import contentBase from "./decorators/contentBase";

@contentBase("Home")
class Home extends React.Component {
	render() {
		return (
			<div>
				Hello world
			</div>
		)
	}
}

export default Home