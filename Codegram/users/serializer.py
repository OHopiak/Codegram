from django.contrib.auth.models import User, Group
from rest_framework import serializers


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ('url', 'first_name', 'last_name', 'username',
				  'email', 'groups', "is_superuser", "is_staff", "is_active",)


class GroupSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Group
		fields = ('url', 'name')
