from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from .views import UserViewSet, GroupViewSet

# schema_view = get_schema_view(title='Pastebin API')

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)

urlpatterns = [
	url(r'^', include(router.urls)),
	# url(r'^schema/$', schema_view),
]
