from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

from Codegram import settings
from Codegram.settings import DEBUG

# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()

urlpatterns = [
	path('api/', include('users.urls')),
	path('api/auth/', include('rest_framework.urls', namespace='rest_framework')),
	path('api/auth/token/', obtain_jwt_token),
	path('api/auth/token/refresh/', refresh_jwt_token),
	path('api/auth/token/verify/', verify_jwt_token),
	url('.*', TemplateView.as_view(template_name="index.html"))
]

if DEBUG:
	url_dev_patterns = [
		path('admin/', admin.site.urls),
	]
	url_dev_patterns += static(settings.ARTIFACTS_URL, document_root=settings.ARTIFACTS_DIR, show_indexes=True)
	urlpatterns = url_dev_patterns + urlpatterns
