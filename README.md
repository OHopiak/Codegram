Codegram
========

This is a web application for managing your projects

Requirements
------------

* python >=3.6 (and requirements.txt dependencies)
* npm >= 5.8.0 (and package.json dependencies)

Usage
-----

To start this project run the following:
```bash
# install requirements
pip install -r requirements.txt
npm -i

# build JS files
npm run build

# or if you want to have constant builds
# run this in other terminal
npm run watch

# prepare the database
./Codegram/manage.py migrate

# start django server
./Codegram/manage.py runserver 8000
```