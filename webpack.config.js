const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const APP_NAME = 'Codegram';
const APP_FILES = APP_NAME + '/static';


const config = {
	entry: path.join(__dirname, APP_FILES + '/src/index.js'),
	output: {
		path: path.join(__dirname, APP_FILES + '/public/build/'),
		filename: 'js/bundle.js'
	},
	resolve: {
		alias: {
			src: path.join(__dirname, APP_FILES + '/src')
		}
	},
	module: {
		rules: [
			{
				test: /\.jsx?/,
				include: path.join(__dirname, APP_FILES + '/'),
				loader: 'babel-loader'
			},
			{
				test: /\.(less|css)$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'less-loader'
				]
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				loader: 'file-loader'
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: 'css/style.css',
			disable: false,
			allChunks: true
		})
	],
};

module.exports = config;